const puppeteer = require('puppeteer');
const got = require('got');

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: {
      width: 1920,
      height: 1080,
    }
  });
  const page = await browser.newPage();
  await page.goto('https://www.serasa.com.br/');

  const [button] = await page.$x("//button[contains(., 'Entrar')]");
  if (button) {
    await button.click();
  }
  await page.waitForSelector('#cpf');
  await page.click('#cpf');
  await page.keyboard.type('12345678900'); // CPF AQUI

  const [submitButton] = await page.$x("//button[contains(., 'Confirmar')]");
  if (submitButton) {
    await submitButton.click();
  }

  await page.waitForSelector('#password');
  await page.click('#password');
  await page.keyboard.type('xxxxxxxxxx'); // SENHA AQUI

  const [submitButton2] = await page.$x("//button[contains(., 'Confirmar')]");
  if (submitButton) {
    await submitButton2.click();
  }

  await page.waitFor(4000);

  const cookies = await page.cookies();
  const tokenCookie = cookies.find(cookie => cookie.name === 'Valgrind-AU');

  const authToken = tokenCookie.value;

  console.log({authToken});

  const response = await got('https://apigw-commons-prd.ecsbr.net/api/home/v1/resume', {
    headers: {
      'x-bifrost-authorization': authToken
    }
  });

  console.log({response})

  await browser.close();
})();
